import '../css/_bootstrap-custom.scss';

import React from 'react';
import ReactDOM from 'react-dom';


class Root extends React.PureComponent {
    render() {
        return (
          <h1>Hello World!</h1>
        );
    }
}


ReactDOM.render(<Root/>, document.getElementById('root'));
