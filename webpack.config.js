/* Everything you need to know about this setup, see video https://youtu.be/8vnkM8JgjpU */

const path = require("path");
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: ["./server_name/static/js/index.js"],
  output: {
    path: path.join(__dirname, "server_name/static/"),
    filename: "js/bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          presets: ['es2015', 'react']
        },
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: [
            'css-loader',
            'sass-loader',
          ],
        }),
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({filename: 'css/bundle.css'})
  ]
};
