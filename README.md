# flask-react-bootstrap-webpack

Basic web server based on flask, react, react-bootstrap, bootstrap-sass and webpack.
This should be used as base skeleton.

## How to setup environment

This project requires *pipenv* and *npm* to setup environment.
```sh
pipenv install
npm install
```

## How to run

Development run with autoreload/autorebuild:
```sh
npm run watch
FLASK_APP = server_name/__main__.py 
FLASK_ENV = development 
python3 -m flask run
```

Production run(with built-in wsgi server):
```sh
npm run build
FLASK_APP = server_name/__main__.py
FLASK_ENV = production
python3 -m flask run
```

## FAQ

Why you include .idea files?
- To simplify setup of my pycharm. And yours also, if you want. Or just delete .idea folder.
